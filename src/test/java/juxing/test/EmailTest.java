package juxing.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.juxing.Application;
import com.juxing.config.EmailService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmailTest {
	
	@Autowired
	private EmailService emailService;
	
	@Test
	public void test1() throws InterruptedException {
		while(true) {
			Thread.sleep(5000);
			emailService.sendHtmlEmail("853516587@qq.com", "主题", "aaaaa");
		}
		
	}
}
