package com.juxing;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import tk.mybatis.spring.annotation.MapperScan;

@MapperScan(basePackages = "com.juxing.mapper")
@EnableTransactionManagement	//开启事务
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext run = new SpringApplicationBuilder(Application.class).run(args);
		
	}

}
