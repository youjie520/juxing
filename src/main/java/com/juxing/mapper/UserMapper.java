package com.juxing.mapper;

import com.juxing.pojo.User;

/**
* 通用 Mapper 代码生成器
*
* @author mapper-generator
*/
public interface UserMapper extends tk.mybatis.mapper.common.Mapper<User> {

}




