package com.juxing.mapper;

import com.juxing.pojo.Express;

/**
* 通用 Mapper 代码生成器
*
* @author mapper-generator
*/
public interface ExpressMapper extends tk.mybatis.mapper.common.Mapper<Express> {

}




