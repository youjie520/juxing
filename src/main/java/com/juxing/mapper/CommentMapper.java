package com.juxing.mapper;

import com.juxing.pojo.Comment;

/**
* 通用 Mapper 代码生成器
*
* @author mapper-generator
*/
public interface CommentMapper extends tk.mybatis.mapper.common.Mapper<Comment> {

}




