package com.juxing.mapper;

import com.juxing.pojo.Product;

/**
* 通用 Mapper 代码生成器
*
* @author mapper-generator
*/
public interface ProductMapper extends tk.mybatis.mapper.common.Mapper<Product> {

}




