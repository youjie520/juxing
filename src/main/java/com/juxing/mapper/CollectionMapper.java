package com.juxing.mapper;

import com.juxing.pojo.Collection;

/**
* 通用 Mapper 代码生成器
*
* @author mapper-generator
*/
public interface CollectionMapper extends tk.mybatis.mapper.common.Mapper<Collection> {

}




