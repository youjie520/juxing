package com.juxing.mapper;

import com.juxing.pojo.Category;

/**
* 通用 Mapper 代码生成器
*
* @author mapper-generator
*/
public interface CategoryMapper extends tk.mybatis.mapper.common.Mapper<Category> {

}




