package com.juxing.controller.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.juxing.myenum.ExpressEnum;
import com.juxing.pojo.Express;
import com.juxing.service.ExpressService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@RestController
@RequestMapping("/client/express")
public class ExpressController {
	
	@Autowired
	private ExpressService expressService;
	
	/**
	 * 根据快递公司id查询快递公司信息
	 * @param expressId
	 * @return
	 */
	@RequestMapping("/byExpressId")
	public Express byExpressId(Long expressId) {
		return expressService.byExpressId(expressId);
	}
	
	/**
	 * 根据条件查询快递公司
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	@RequestMapping("/list")
	public List<Express> listExpress(@RequestParam(defaultValue="1") Integer pageNum, 
									 @RequestParam(defaultValue="99999999")Integer pageSize) {
		// 按type类型排序，并且查询支持的快递公司
		Example example = new Example(Express.class);
		example.setOrderByClause("type asc");
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("status", ExpressEnum.NORMAL.getCode());
		return expressService.listExpress(pageNum, pageSize, example);
	}
}
