package com.juxing.controller.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.juxing.config.WXAccessTokenUtils;

@RestController
@RequestMapping("/accessToken")
public class AccessToken {

	public static Logger LOG = LoggerFactory.getLogger(AccessToken.class);

	@Autowired
	private WXAccessTokenUtils wxAccessTokenUtils;

	@RequestMapping("/get")
	public void get(HttpServletResponse response) throws IOException {
		OutputStream outputStream = response.getOutputStream();
		wxAccessTokenUtils.getAccessToken();
		InputStream inputStream = getminiqrQr("youjie",
				"15_l6LIZ4o_5tTc3ATM8buEosIeqLjtCsUGkIXwIls4FQIAaglspVe_StoAiKBPk_KOYhe4zyYnstDvZ-CgsHVyYxoxpCNpAkJF0G7FOQFKtvY8QBeAB9jJjiKsC5ikJ2UIK14OURqbEoMz5WcBUANcAJAQWJ");
//		outputStream = new FileOutputStream(file);
		int len = 0;
		byte[] buf = new byte[1024];
		while ((len = inputStream.read(buf, 0, 1024)) != -1) {
			outputStream.write(buf, 0, len);
		}
		outputStream.flush();

	}

	public InputStream getminiqrQr(String sceneStr, String accessToken) {
		RestTemplate rest = new RestTemplate();
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			String url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + accessToken;
			Map<String, Object> param = new HashMap<>();
			param.put("scene", sceneStr);
			// param.put("page", "pages/index/index");
			param.put("width", 430);
			param.put("auto_color", false);
			Map<String, Object> line_color = new HashMap<>();
			line_color.put("r", 0);
			line_color.put("g", 0);
			line_color.put("b", 0);
			param.put("line_color", line_color);
			LOG.info("调用生成微信URL接口传参:" + param);
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			HttpEntity requestEntity = new HttpEntity(param, headers);
			ResponseEntity<byte[]> entity = rest.exchange(url, HttpMethod.POST, requestEntity, byte[].class,
					new Object[0]);
			LOG.info("调用小程序生成微信永久小程序码URL接口返回结果:" + entity.getBody());
			byte[] result = entity.getBody();
//			LOG.info(Base64.encodeBase64String(result));
			inputStream = new ByteArrayInputStream(result);

			// File file = new File("C:\\Users\\Administrator\\Desktop\\1.png");
			// if (!file.exists()){
			// file.createNewFile();
			// }
			// outputStream = new FileOutputStream(file);
			// int len = 0;
			// byte[] buf = new byte[1024];
			// while ((len = inputStream.read(buf, 0, 1024)) != -1) {
			// outputStream.write(buf, 0, len);
			// }
			// outputStream.flush();
			return inputStream;
		} catch (Exception e) {
			LOG.error("调用小程序生成微信永久小程序码URL接口异常", e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
