package com.juxing.controller.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.juxing.config.EmailService;
import com.juxing.pojo.SendExpress;
import com.juxing.pojo.User;
import com.juxing.service.SendExpressService;
import com.juxing.utils.ResResult;
import com.juxing.utils.UserRedisUtil;

@RestController
@RequestMapping("/client/sendExpress")
public class SendExpressController {

	@Autowired
	private UserRedisUtil userRedisUtil;
	@Autowired
	private SendExpressService sendExpressService;
	@Autowired
	private EmailService emailService;

	/**
	 * 创建新的寄快递
	 * 
	 * @param sendExpress
	 * @return
	 */
	@RequestMapping("/insert")
	public ResResult insert(String token,@Validated SendExpress sendExpress) {
		// 获取登录用户
		User loginUser = userRedisUtil.getUser(token);
		sendExpress.setUserId(loginUser.getUserId());
		emailService.sendHtmlEmail("807400947@qq.com", "用户需要寄快递", sendExpress.toString());
		return ResResult.ok(sendExpressService.insert(sendExpress));
	}
}
