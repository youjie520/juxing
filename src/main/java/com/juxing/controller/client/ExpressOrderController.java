package com.juxing.controller.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.juxing.pojo.ExpressOrder;
import com.juxing.pojo.User;
import com.juxing.povo.ExpressOrderVo;
import com.juxing.service.ExpressOrderService;
import com.juxing.utils.ResResult;
import com.juxing.utils.UserRedisUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 快递订单
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/client/expressOrder")
public class ExpressOrderController {
	
	@Autowired
	private ExpressOrderService expressOrderService;
	@Autowired
	private UserRedisUtil userRedisUtil;
	
	/**
	 * 添加送快递订单
	 * @param token
	 * @param expressOrder
	 * @return
	 */
	@RequestMapping("/addExpressOrder")
	public ResResult addExpressOrder(String token,ExpressOrder expressOrder) {
		User loginUser = userRedisUtil.getUser(token);
		expressOrder.setUserId(loginUser.getUserId());
		expressOrderService.newExpressOrder(expressOrder);
		return ResResult.ok();
	}
	
	/**
	 * 根据订单状态查询用户的订单
	 * @param pageNum
	 * @param pageSize
	 * @param token
	 * @param status
	 * @return
	 */
	@RequestMapping("/myExpressOrder")
	public ResResult myExpressOrder(@RequestParam(defaultValue="1") Integer pageNum, 
			 						@RequestParam(defaultValue="10")Integer pageSize,
			 						String token,Integer status) {
		User loginUser = userRedisUtil.getUser(token);
		
		Example example = new Example(ExpressOrder.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("userId", loginUser.getUserId());
		criteria.andEqualTo("status", status);
		
		PageInfo<ExpressOrderVo> list = expressOrderService.list(pageNum, pageSize, example);
		return ResResult.ok(list);
	}
	/**
	 * 用户取消订单
	 * @param token
	 * @return
	 */
	@RequestMapping("/orderCancel")
	public ResResult orderCancel(String token,Long ExpressOrderId) {
		User loginUser = userRedisUtil.getUser(token);
		return ResResult.ok(expressOrderService.orderCancel(ExpressOrderId, null,loginUser,false));
	}
	
}
