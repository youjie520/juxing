package com.juxing.controller.client;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.juxing.myenum.ProductEnum;
import com.juxing.pojo.Comment;
import com.juxing.pojo.Product;
import com.juxing.pojo.User;
import com.juxing.povo.CommentVo;
import com.juxing.povo.ProductVo;
import com.juxing.service.ProductService;
import com.juxing.utils.ResResult;
import com.juxing.utils.UserRedisUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@RestController
@RequestMapping("/client/product")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	@Autowired
	private UserRedisUtil userRedisUtil;
	
	
	/**
	 * 用户添加商品
	 * @param token
	 * @param product
	 * @return
	 */
	@RequestMapping("/addProduct")
	public ResResult addProduct(String token,@Validated Product product) {
		User user = userRedisUtil.getUser(token);
		product.setUserId(user.getUserId());
		int res = productService.insertProduct(product);
		if(res>=1) {
			return ResResult.ok();
		}
		return ResResult.build(ProductEnum.ADD_REEOR.getCode(), ProductEnum.ADD_REEOR.getMessage());
	}
	
	/**
	 * 卖家更新商品[根据商品id和用户id查询商品]
	 */
	@RequestMapping("/userUpdateProduct")
	public ResResult userUpdateProduct(String token,@Validated  Product product){
		if(product.getProductId()==null) {
			return ResResult.build(400, "商品id没用");
		}
		User user = userRedisUtil.getUser(token);	
		product.setUserId(user.getUserId());
		return ResResult.ok(productService.userUpdateProduct(product));
	}
	
	/**
	 * 下架商品  [通过商品id和用户id]
	 * @param token
	 * @param product
	 * @return
	 */
	@RequestMapping("/downProduct")
	public ResResult downProduct(String token,Product product) {
		if(product.getProductId()==null) {
			return ResResult.build(400, "商品id没用");
		}
		User user = userRedisUtil.getUser(token);	
		product.setUserId(user.getUserId());
		return ResResult.ok(productService.downProduct(product));
	}
	
	/**
	 * 根据商品id获取商品详细[商品发布者详细，商品分类]
	 * @param ProductId
	 * @return
	 */
	@RequestMapping("/byProductId/{productId}")
	public ResResult byProductId(@PathVariable String productId) {
		ProductVo productVo = productService.byId(productId);
		return ResResult.ok(productVo);
	}
	
	/**
	 * 添加评论
	 * @param token
	 * @param comment
	 * @return
	 */
	@RequestMapping("/addComment")
	public ResResult addComment(String token,Comment comment) {
		ResResult resResult = null;
		if(StringUtils.isEmpty(comment.getContent())) {
			resResult = ResResult.build(400, "评论失败");
		}
		User user = userRedisUtil.getUser(token);
		comment.setUserId(user.getUserId());
		int res = productService.addComment(comment);
		if(res>=1) {
			resResult = ResResult.ok();
			
		}else {
			resResult = ResResult.build(400, "评论失败");
		}
		return resResult;
	}
	
	@RequestMapping("/commentList")
	public ResResult commentList(@RequestParam(defaultValue = "1") Integer pageNum,
			@RequestParam(defaultValue = "10") Integer pageSize,
			String productId) {
		PageInfo<CommentVo> page = productService.commentList(pageNum, pageSize, productId);
		return ResResult.ok(page);
		
	}
	
	/**
	 * 获取商品列表
	 * @param pageNum
	 * @param pageSize
	 * @param product 条件
	 * @param order 排序方式
	 * @return
	 */
	@RequestMapping("/list")
	public ResResult list(  @RequestParam(defaultValue = "1") Integer pageNum,
							@RequestParam(defaultValue = "10") Integer pageSize,
							Product product,@RequestParam(defaultValue = "1") Integer order) {
		Example example = new Example(Product.class);
		Criteria criteria = example.createCriteria();
		
		if(product.getCategoryId()!=null) {
			criteria.andEqualTo("categoryId", product.getCategoryId());
		}
		if(product.getUserId()!=null) {
			criteria.andEqualTo("userId", product.getUserId());
		}
		if(!StringUtils.isEmpty(product.getName())) {
			criteria.andLike("name", "%"+product.getName()+"%");
		}
		
//		只能是自销或商品审核通过的
		List<Integer> status = new ArrayList<Integer>();
		status.add(ProductEnum.SELF_SELLING.getCode());
		status.add(ProductEnum.PASS.getCode());
		criteria.andIn("status", status);
		
		return ResResult.ok(productService.list(pageNum, pageSize, example));
	}
}
