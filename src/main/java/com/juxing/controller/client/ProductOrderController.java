package com.juxing.controller.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.juxing.myenum.ProductEnum;
import com.juxing.myenum.ProductOrderEnum;
import com.juxing.pojo.ProductOrder;
import com.juxing.pojo.User;
import com.juxing.povo.ProductOrderVo;
import com.juxing.service.ProductOrderService;
import com.juxing.utils.ResResult;
import com.juxing.utils.UserRedisUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@RestController
@RequestMapping("/client/productOrder")
public class ProductOrderController {
	@Autowired
	private UserRedisUtil userRedisUtil;

	@Autowired
	private ProductOrderService productOrderService;

	@RequestMapping("/newOrder")
	public ResResult newOrder(String token, ProductOrder productOrder) {
		// 获取登录用户
		User loginUser = userRedisUtil.getUser(token);
		productOrder.setUserId(loginUser.getUserId());
		return ResResult.ok(productOrderService.insertProductOrder(productOrder));
	}

	/**
	 * 用户取消订单
	 * @param token
	 * @param productOrderId
	 * @return
	 */
	@RequestMapping("/cancel")
	public ResResult cancel(String token, String productOrderId) {
		// 获取登录用户
		User loginUser = userRedisUtil.getUser(token);
		ProductOrder productOrder = new ProductOrder();
		//设置订单是登录用户的
		productOrder.setUserId(loginUser.getUserId());
		productOrder.setProductOrderId(productOrderId);
		
		return ResResult.ok(productOrderService.userCancel(productOrder));
	}

	@RequestMapping("/list")
	public ResResult list(String token, @RequestParam(defaultValue = "1") Integer pageNum,
			@RequestParam(defaultValue = "10") Integer pageSize, Integer status) {
		// 获取登录用户
		User loginUser = userRedisUtil.getUser(token);
		Example example = new Example(ProductOrder.class);
		example.setOrderByClause("create_date DESC");
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("userId", loginUser.getUserId());
		criteria.andEqualTo("status", status);

		PageInfo<ProductOrderVo> page = productOrderService.list(pageNum, pageSize, example);

		return ResResult.ok(page);
	}
}
