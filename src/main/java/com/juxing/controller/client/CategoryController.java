package com.juxing.controller.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.juxing.myenum.ExpressEnum;
import com.juxing.pojo.Category;
import com.juxing.service.CategoryService;
import com.juxing.utils.ResResult;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@RestController
@RequestMapping("/client/category")
public class CategoryController {

	@Autowired
	private CategoryService CategoryService;
	
	/**
	 * 获取上架的分类
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/all")
	public ResResult all(@RequestParam(defaultValue="1") Integer pageNum, 
			 @RequestParam(defaultValue="99999999")Integer pageSize) {
		Example example = new Example(Category.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("status", ExpressEnum.NORMAL.getCode());
		PageInfo<Category> list = CategoryService.selectByExample(pageNum, pageSize, example);
		return ResResult.ok(list);
	}
}
