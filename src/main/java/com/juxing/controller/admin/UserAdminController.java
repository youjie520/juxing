package com.juxing.controller.admin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.juxing.myenum.UserEnum;
import com.juxing.pojo.User;
import com.juxing.service.UserService;
import com.juxing.utils.JsonUtils;
import com.juxing.utils.ResResult;
import com.juxing.utils.UserRedisUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@RestController
@RequestMapping("/admin/user")
public class UserAdminController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserRedisUtil userRedisUtil;
	@Autowired
	private StringRedisTemplate redisTemplate;
	
	@RequestMapping("/login")
	public ResResult login(User user) {
		User dbUser = userService.byUserObj(user);
		if (dbUser == null) {
			return ResResult.build(UserEnum.PHONE_PASSWORD_ERROR.getCode(), UserEnum.PHONE_PASSWORD_ERROR.getMessage());
		}
		dbUser.setPassword(null);
		// 生成token
		String token = UUID.randomUUID().toString();
		// 保存到redis
		redisTemplate.opsForValue().set("user:" + token, JsonUtils.objectToJson(dbUser), 86400, TimeUnit.SECONDS); // 保存1天
		// 有返回 为null也直接返回了
		Map<String, Object> map = new HashMap<>();
		map.put("userInfo", dbUser);
		map.put("token", "user:" + token);
		return ResResult.ok(map);
	}
	
	/**
	 * 退出登录
	 * @param token
	 * @return
	 */
	@RequestMapping("/outLogin")
	public ResResult outLogin(String token) {
		Boolean isDel = redisTemplate.delete(token);
		return ResResult.ok(isDel);
	}
	
	/**
	 * 根据条件查询用户
	 * @param pageNum
	 * @param pageSize
	 * @param user
	 * @return
	 */
	@RequestMapping("/list")
	public ResResult list(@RequestParam(defaultValue = "1") Integer pageNum,
			@RequestParam(defaultValue = "10") Integer pageSize, User user) {
		Example example =  new Example(User.class);
		example.setOrderByClause("status DESC");
		Criteria criteria = example.createCriteria();
		if(user.getUserId()!=null) {
			criteria.andEqualTo("userId", user.getUserId());
		}
		if(!StringUtils.isEmpty(user.getName())) {
			criteria.andLike("name", "%"+user.getName()+"%");
		}
		if(!StringUtils.isEmpty(user.getPhone())) {
			criteria.andLike("phone", "%"+user.getPhone()+"%");
		}
		if(!StringUtils.isEmpty(user.getDormNumber())) {
			criteria.andLike("dormNumber", "%"+user.getDormNumber()+"%");
		}
		if(user.getStatus()!=null) {
			criteria.andEqualTo("status", user.getStatus());
		}
		
		return ResResult.ok(userService.listUser(pageNum, pageSize, example));
	}
	
	/**
	 * 超级管理员更新用户
	 * @param requset
	 * @param user
	 * @return
	 */
	@RequestMapping("/updatsUser")
	public ResResult updatsUser(HttpServletRequest requset,User user) {
		User loginUser = userRedisUtil.getUser(requset.getHeader("token"));
		if(loginUser!=null && loginUser.getStatus()==UserEnum.Leve5.getCode()) {
			userService.updateUser(user);
			return ResResult.ok();
		}
		return ResResult.build(UserEnum.OP_REEOR.getCode(), UserEnum.OP_REEOR.getMessage());
	}
	
	/**
	 * 获取所有管理员
	 * @return
	 */
	@RequestMapping("/adminList")
	public ResResult adminList(){
		Example example =  new Example(User.class);
		Criteria criteria = example.createCriteria();
		criteria.orEqualTo("status", UserEnum.Leve4.getCode());
		criteria.orEqualTo("status", UserEnum.Leve5.getCode());
		return ResResult.ok(userService.listUser(1, 999999, example));
		
	}
}
