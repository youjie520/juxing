package com.juxing.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.juxing.pojo.SendExpress;
import com.juxing.service.SendExpressService;
import com.juxing.utils.ResResult;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@RestController
@RequestMapping("/admin/sendExpress")
public class SendExpressAdminController {
	
	@Autowired
	private SendExpressService sendExpressService;
	
	/** 
	 * 取消订单
	 */
	@RequestMapping("/cancel")
	public ResResult cancel(String sendExpressId) {
		return ResResult.ok(sendExpressService.cancel(sendExpressId));
	}
	/**
	 * 揽件
	 */
	@RequestMapping("/lj")
	public ResResult lj(String sendExpressId) {
		return ResResult.ok(sendExpressService.lj(sendExpressId));
	}
	/**
	 * 寄件完成
	 */
	@RequestMapping("/jjSuccess")
	public ResResult jjSuccess(String sendExpressId) {
		return ResResult.ok(sendExpressService.jjSuccess(sendExpressId));
	}
	
	
	@RequestMapping("/list")
	public ResResult list(@RequestParam(defaultValue = "1") Integer pageNum,
			@RequestParam(defaultValue = "10") Integer pageSize, SendExpress sendExpress) {
		Example example = new Example(SendExpress.class);
		Criteria criteria = example.createCriteria();
		if(sendExpress.getStatus()!=null){
			criteria.andEqualTo("status", sendExpress.getStatus());
		}
		if(sendExpress.getSendExpressId()!=null){
			criteria.andLike("sendExpressId", "%"+sendExpress.getSendExpressId()+"%");
		}
		if(sendExpress.getExpressId()!=null){
			criteria.andEqualTo("expressId", sendExpress.getExpressId());
		}
		
		return ResResult.ok(sendExpressService.list(pageNum, pageSize, example));
	}
	
}
