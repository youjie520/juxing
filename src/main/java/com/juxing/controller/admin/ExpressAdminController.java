package com.juxing.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.juxing.myenum.UserEnum;
import com.juxing.pojo.Express;
import com.juxing.pojo.User;
import com.juxing.service.ExpressService;
import com.juxing.utils.ResResult;
import com.juxing.utils.UserRedisUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@RestController
@RequestMapping("/admin/express")
public class ExpressAdminController {

	@Autowired
	private ExpressService expressService;
	@Autowired
	private UserRedisUtil userRedisUtil;
	@Autowired
	private StringRedisTemplate redisTemplate;
	
	@RequestMapping("/add")
	public ResResult add(HttpServletRequest requset,Express express) {
		User loginUser = userRedisUtil.getUser(requset.getHeader("token"));
		if(loginUser!=null && loginUser.getStatus()==UserEnum.Leve5.getCode()) {
			return ResResult.ok(expressService.insertExpress(express));
		}
		return ResResult.build(UserEnum.OP_REEOR.getCode(), UserEnum.OP_REEOR.getMessage());
	}
	
	@RequestMapping("/update")
	public ResResult update(HttpServletRequest requset,Express express) {
		User loginUser = userRedisUtil.getUser(requset.getHeader("token"));
		if(loginUser!=null && loginUser.getStatus()==UserEnum.Leve5.getCode()) {
			return ResResult.ok(expressService.updateExpress(express));
		}
		return ResResult.build(UserEnum.OP_REEOR.getCode(), UserEnum.OP_REEOR.getMessage());
	}
	
	/**
	 * 根据条件查询快递公司
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	@RequestMapping("/list")
	public List<Express> listExpress(@RequestParam(defaultValue="1") Integer pageNum, 
									 @RequestParam(defaultValue="99999999")Integer pageSize) {
		// 按type类型排序，并且查询支持的快递公司
		Example example = new Example(Express.class);
		example.setOrderByClause("type asc");
		Criteria criteria = example.createCriteria();
		return expressService.listExpress(pageNum, pageSize, example);
	}
}
