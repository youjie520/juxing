package com.juxing.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.juxing.pojo.Product;
import com.juxing.pojo.ProductOrder;
import com.juxing.pojo.User;
import com.juxing.povo.ProductOrderVo;
import com.juxing.service.ProductOrderService;
import com.juxing.service.ProductService;
import com.juxing.utils.ResResult;
import com.juxing.utils.UserRedisUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@RestController
@RequestMapping("/admin/product")
public class ProductAdminController {
	
	@Autowired
	private UserRedisUtil userRedisUtil;
	@Autowired
	private ProductService productService;
	
	/**
	 * 管理员收到货物
	 * @param productId
	 * @return
	 */
	@RequestMapping("/takeOver")
	public ResResult takeOver(String productId) {
		return ResResult.ok(productService.takeOver(productId));
	} 

	@RequestMapping("/list")
	public ResResult list(String token, @RequestParam(defaultValue = "1") Integer pageNum,
			@RequestParam(defaultValue = "10") Integer pageSize, Product product) {
		Example example = new Example(Product.class);
		example.setOrderByClause("create_date DESC");
		Criteria criteria = example.createCriteria();
		
		if(!StringUtils.isEmpty(product.getProductId())) {
			criteria.andLike("productId", "%"+product.getProductId()+"%");
		}
		if(product.getCategoryId()!=null) {
			criteria.andEqualTo("categoryId", product.getCategoryId());
		}
		if(product.getUserId()!=null) {
			criteria.andEqualTo("userId", product.getUserId());
		}
		if(!StringUtils.isEmpty(product.getName())) {
			criteria.andLike("name", "%"+product.getName()+"%");
		}
		if(product.getStatus()!=null) {
			criteria.andEqualTo("status", product.getStatus());
		}
		
		PageInfo<Product> page = productService.list(pageNum, pageSize, example);

		return ResResult.ok(page);
	}
}
