package com.juxing.exception;

import com.juxing.myenum.UserEnum;

public class UserException extends BaseException{

	public UserException(String message, Integer code) {
		super(message, code);
	}
	
	public UserException(UserEnum userEnum) {
		super(userEnum.getMessage(), userEnum.getCode());
	}

}
