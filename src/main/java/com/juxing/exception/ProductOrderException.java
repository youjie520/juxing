package com.juxing.exception;

import com.juxing.myenum.ProductOrderEnum;

public class ProductOrderException extends BaseException{

	public ProductOrderException(String message, Integer code) {
		super(message, code);
	}
	
	public ProductOrderException(ProductOrderEnum productOrderEnum) {
		super(productOrderEnum.getMessage(), productOrderEnum.getCode());
	}

}
