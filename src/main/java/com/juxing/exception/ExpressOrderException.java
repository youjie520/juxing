package com.juxing.exception;

import com.juxing.myenum.ExpressOrderEnum;

public class ExpressOrderException extends BaseException{

	public ExpressOrderException(String message, Integer code) {
		super(message, code);
	}
	
	public ExpressOrderException(ExpressOrderEnum expressOrderEnum) {
		super(expressOrderEnum.getMessage(), expressOrderEnum.getCode());
	}

}
