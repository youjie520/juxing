package com.juxing.exception;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.juxing.utils.ResResult;

@ControllerAdvice
public class GlobalException {
	
    //    @ExceptionHandler(value={RuntimeException.class,MyRuntimeException.class})
    //    @ExceptionHandler//处理所有异常
	@ExceptionHandler(RuntimeException.class)
    @ResponseBody //在返回自定义相应类的情况下必须有，这是@ControllerAdvice注解的规定
  //返回的状态码
//    @ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)     //服务内部错误
    public ResResult exceptionHandler(RuntimeException e, HttpServletResponse response) {
		ResResult resp = null;
		e.printStackTrace();
		if(e instanceof BaseException) {
			BaseException ex= (BaseException)e;
			resp = ResResult.build(ex.getCode(), ex.getMessage());
//			response.setStatus(600); // 自定义错误
		}else {
			resp =ResResult.build(500, e.getMessage());
			response.setStatus(500); // 系统错误
		}
        return resp;
    }
}
