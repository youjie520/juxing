package com.juxing.exception;

import com.juxing.myenum.ProductEnum;

public class ProductException extends BaseException{

	public ProductException(String message, Integer code) {
		super(message, code);
	}
	
	public ProductException(ProductEnum productEnum) {
		super(productEnum.getMessage(), productEnum.getCode());
	}

}
