package com.juxing.service;

import com.github.pagehelper.PageInfo;
import com.juxing.pojo.SendExpress;

import tk.mybatis.mapper.entity.Example;

public interface SendExpressService {
	
	/**
	 * 创建新的寄快递
	 * @param sendExpress
	 * @return
	 */
	public int insert(SendExpress sendExpress);
	
	/** 
	 * 取消订单
	 */
	public int cancel(String sendExpressId);
	
	/**
	 * 揽件
	 */
	public int lj(String sendExpressId);
	
	/**
	 * 寄件完成
	 */
	public int jjSuccess(String sendExpressId);
	
	/**
	 * 查询寄快递列表
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	public PageInfo<Object> list(Integer pageNum, Integer pageSize, Example example);
}
