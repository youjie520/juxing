package com.juxing.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.juxing.exception.ExpressOrderException;
import com.juxing.mapper.ExpressMapper;
import com.juxing.mapper.ExpressOrderMapper;
import com.juxing.mapper.UserMapper;
import com.juxing.myenum.ExpressEnum;
import com.juxing.myenum.ExpressOrderEnum;
import com.juxing.pojo.Express;
import com.juxing.pojo.ExpressOrder;
import com.juxing.pojo.User;
import com.juxing.povo.ExpressOrderVo;
import com.juxing.service.ExpressOrderService;
import com.juxing.utils.IDUtil;

import tk.mybatis.mapper.entity.Example;

@Service
public class ExpressOrderServiceImpl implements ExpressOrderService {

	@Autowired
	private ExpressOrderMapper expressOrderMapper;
	@Autowired
	private ExpressMapper expressMapper;
	@Autowired
	private UserMapper userMapper;

	/**
	 * 申请 帮拿快递订单
	 * 
	 * @param expressOrder
	 * @return
	 */
	public int newExpressOrder(ExpressOrder expressOrder) {

		int selectCount = expressOrderMapper.selectCount(expressOrder);
		if (selectCount > 0) {// 有重复的
			// 用户重复提交
			throw new ExpressOrderException(ExpressOrderEnum.REPEAT_SUBMIT);
		}
		// 快递公司
		Express dbExpress = expressMapper.selectByPrimaryKey(expressOrder.getExpressId());
		User dbUser = userMapper.selectByPrimaryKey(expressOrder.getUserId());
		if (dbExpress.getType() > dbUser.getStatus()) {
			// 用户不能申请这个快递公司类型
			throw new ExpressOrderException(ExpressOrderEnum.NOT_AUTHORITY);
		}
		// 11为支持 10为不支持该快递公司
		if(dbExpress.getStatus()==ExpressEnum.OFFNORMAL.getCode()) {
			throw new ExpressOrderException(ExpressOrderEnum.SUPPORT_ERROR);
		}
		expressOrder.setOrderId(IDUtil.getId());
		expressOrder.setStatus(ExpressOrderEnum.WAIT_DISTRIBUTION.getCode());
		expressOrder.setCancelDesc(null);

		return expressOrderMapper.insert(expressOrder);
	}

	/**
	 * 管理员配送中
	 * 
	 * @param ExpressOrder
	 * @param user
	 *            管理员
	 * @return
	 */
	public int orderDistribution(Long ExpressOrderId, User adminUser) {
		ExpressOrder dbExpressOrder = expressOrderMapper.selectByPrimaryKey(ExpressOrderId);
		if (dbExpressOrder == null) {
			throw new ExpressOrderException(ExpressOrderEnum.NOT_EXPRESS_ORDER);
		}
		if (dbExpressOrder.getStatus() != ExpressOrderEnum.WAIT_DISTRIBUTION.getCode()) {
			// 配送订单必须是等待配送状态
			// 如果不是则异常
			throw new ExpressOrderException(ExpressOrderEnum.STATUS_ERROR);
		}
		dbExpressOrder.setAdminId(adminUser.getUserId());
		dbExpressOrder.setStatus(ExpressOrderEnum.DISTRIBUTION.getCode());
		return expressOrderMapper.updateByPrimaryKeySelective(dbExpressOrder);
	}

	/**
	 * 管理员配送完成
	 * 
	 * @param ExpressOrder
	 * @return
	 */
	public int orderSuccess(Long ExpressOrderId, User adminUser) {
		ExpressOrder dbExpressOrder = expressOrderMapper.selectByPrimaryKey(ExpressOrderId);
		if (dbExpressOrder == null) {
			throw new ExpressOrderException(ExpressOrderEnum.NOT_EXPRESS_ORDER);
		}
		if (dbExpressOrder.getStatus() != ExpressOrderEnum.DISTRIBUTION.getCode()) {
			// 配送订单必须是等待配送状态
			// 如果不是则异常
			throw new ExpressOrderException(ExpressOrderEnum.STATUS_ERROR);
		}
		if (!dbExpressOrder.getAdminId().equals(adminUser.getUserId())) {
			// 必须是接单的管理员才能完成
			throw new ExpressOrderException(ExpressOrderEnum.ORDER_ADMIN_ERROR);
		}
		Express express = expressMapper.selectByPrimaryKey(dbExpressOrder.getExpressId());
		express.setNumber(express.getNumber()+1);
		expressMapper.updateByPrimaryKeySelective(express);
		dbExpressOrder.setStatus(ExpressOrderEnum.SUCCESS_DISTRIBUTION.getCode());
		return expressOrderMapper.updateByPrimaryKeySelective(dbExpressOrder);
	}

	/**
	 * 管理员取消订单
	 * 
	 * @param ExpressOrderId
	 * @param cancelMsg
	 *            // 管理员取消理由
	 * @param user
	 * @param isAdmin
	 *            true管理员 fase用户
	 * @return
	 */
	public int orderCancel(Long ExpressOrderId, String cancelMsg, User user, boolean isAdmin) {
		ExpressOrder dbExpressOrder = expressOrderMapper.selectByPrimaryKey(ExpressOrderId);
		if (dbExpressOrder == null) {
			throw new ExpressOrderException(ExpressOrderEnum.NOT_EXPRESS_ORDER);
		}
		if (isAdmin == false) {
			// 用户取消
			if (dbExpressOrder.getStatus() != ExpressOrderEnum.WAIT_DISTRIBUTION.getCode()) {
				// 用户取消必须是等待配送状态
				// 如果不是则异常
				throw new ExpressOrderException(ExpressOrderEnum.STATUS_ERROR);
			}
			// 是不是登录用户的订单 //1539569819608
			if (!dbExpressOrder.getUserId().equals(user.getUserId())) { // 1539569819608
				throw new ExpressOrderException(ExpressOrderEnum.NOT_USER);
			}
		} else {
			if (dbExpressOrder.getAdminId() != null) {
				// 管理员取消
				if (!dbExpressOrder.getAdminId().equals(user.getUserId())) {
					// 必须是接单的管理员才能完成
					throw new ExpressOrderException(ExpressOrderEnum.ORDER_ADMIN_ERROR);
				}
			}
			// 如果没有管理员管理该订单可以直接取消
			dbExpressOrder.setCancelDesc(cancelMsg);
		}
		dbExpressOrder.setStatus(ExpressOrderEnum.CANCEL_DISTRIBUTION.getCode());
		return expressOrderMapper.updateByPrimaryKeySelective(dbExpressOrder);
	}

	/**
	 * 更新快递订单
	 * 
	 * @param expressOrder
	 * @return
	 */
	public Integer update(ExpressOrder expressOrder, User loginUser) {
		ExpressOrder dbExpressOrder = expressOrderMapper.selectByPrimaryKey(expressOrder.getOrderId());
		if (dbExpressOrder == null) {
			throw new ExpressOrderException(ExpressOrderEnum.NOT_EXPRESS_ORDER);
		}
		if (dbExpressOrder.getAdminId() != null) {
			// 判断是当前订单否是是当前管理员管理的
			if (!dbExpressOrder.getAdminId().equals(loginUser.getUserId())) {
				// 必须是接单的管理员才能完成
				throw new ExpressOrderException(ExpressOrderEnum.ORDER_ADMIN_ERROR);
			}
		}
		dbExpressOrder.setExpressId(expressOrder.getExpressId());
		dbExpressOrder.setPickUpCode(expressOrder.getPickUpCode());
		dbExpressOrder.setName(expressOrder.getName());
		dbExpressOrder.setPhone(expressOrder.getPhone());
		dbExpressOrder.setDormNumber(expressOrder.getDormNumber());
		dbExpressOrder.setAdminId(loginUser.getUserId());
		
		return expressOrderMapper.updateByPrimaryKeySelective(dbExpressOrder);
	}

	/**
	 * 根据订单id获取订单详细
	 * 
	 * @param orderId
	 * @return
	 */
	public ExpressOrderVo byOrderId(Long orderId) {
		ExpressOrderVo expressOrderVo = new ExpressOrderVo();

		ExpressOrder dbexpressOrder = expressOrderMapper.selectByPrimaryKey(orderId);
		BeanUtils.copyProperties(dbexpressOrder, expressOrderVo);
		// 快递公司
		Express dbexpress = expressMapper.selectByPrimaryKey(dbexpressOrder.getExpressId());
		expressOrderVo.setExpress(dbexpress);
		// 快递用户
		User dbuser = userMapper.selectByPrimaryKey(dbexpressOrder.getUserId());
		dbuser.setPassword(null);
		expressOrderVo.setUser(dbuser);
		// 管理员
		User dbAdminUser = userMapper.selectByPrimaryKey(dbexpressOrder.getAdminId());
		dbAdminUser.setPassword(null);
		expressOrderVo.setAdminUser(dbAdminUser);
		return expressOrderVo;
	}

	/**
	 * 根据条件查询订单
	 * 
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	public PageInfo<ExpressOrderVo> list(Integer pageNum, Integer pageSize, Example example) {
		// 保存包装后的订单
		List<ExpressOrderVo> expressOrderVos = new ArrayList<>();
		// 查询符合要求的的订单
		PageHelper.startPage(pageNum, pageSize);
		List<ExpressOrder> dbOrders = expressOrderMapper.selectByExample(example);
		// 遍历包装
		for (ExpressOrder temp : dbOrders) {
			expressOrderVos.add(this.fill(temp));
		}
		PageInfo<ExpressOrder> pageInfo1 = new PageInfo<>(dbOrders);
		PageInfo<ExpressOrderVo> pageInfo2 = new PageInfo<>();

		BeanUtils.copyProperties(pageInfo1, pageInfo2);
		pageInfo2.setList(expressOrderVos);
		return pageInfo2;
	}

	/**
	 * 填充数据 (快递公司,快递用户，管理员)
	 * 
	 * @param expressOrder
	 * @return
	 */
	public ExpressOrderVo fill(ExpressOrder expressOrder) {

		ExpressOrderVo expressOrderVo = new ExpressOrderVo();
		BeanUtils.copyProperties(expressOrder, expressOrderVo);
		// 快递公司
		Express dbexpress = expressMapper.selectByPrimaryKey(expressOrder.getExpressId());
		expressOrderVo.setExpress(dbexpress);
		// 快递用户
		User dbuser = userMapper.selectByPrimaryKey(expressOrder.getUserId());
		dbuser.setPassword(null);
		expressOrderVo.setUser(dbuser);
		// 管理员
		User dbAdminUser = userMapper.selectByPrimaryKey(expressOrder.getAdminId());
		if (dbAdminUser != null) {
			dbAdminUser.setPassword(null);
			expressOrderVo.setAdminUser(dbAdminUser);
		}
		return expressOrderVo;
	}

}
