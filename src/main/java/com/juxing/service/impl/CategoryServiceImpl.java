package com.juxing.service.impl;

import org.springframework.stereotype.Service;

import com.juxing.pojo.Category;
import com.juxing.service.CategoryService;

@Service
public class CategoryServiceImpl extends BaseService<Category> implements CategoryService {

}
