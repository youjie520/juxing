package com.juxing.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.juxing.mapper.ExpressMapper;
import com.juxing.myenum.ExpressEnum;
import com.juxing.pojo.Express;
import com.juxing.service.ExpressService;
import com.juxing.utils.IDUtil;

import tk.mybatis.mapper.entity.Example;

@Service
public class ExpressServiceImpl implements ExpressService{
	
	@Autowired
	private ExpressMapper expressMapper;
	/**
	 * 添加快递公司
	 * @param express
	 * @return
	 */
	@Override
	public int insertExpress(Express express) {
		express.setExpressId(IDUtil.getId());
		express.setNumber(0);
		express.setStatus(ExpressEnum.NORMAL.getCode());
		return expressMapper.insert(express);
	}

	/**
	 * 更新快递公司 (status==0不支持该快递公司)
	 * @param express
	 * @return
	 */
	@Override
	public int updateExpress(Express express) {
		return expressMapper.updateByPrimaryKeySelective(express);
	}

	/**
	 * 根据快递公司id查询快递公司信息
	 * @param expressId
	 * @return
	 */
	@Override
	public Express byExpressId(Long expressId) {
		return expressMapper.selectByPrimaryKey(expressId);
	}

	/**
	 * 根据条件查询快递公司
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	@Override
	public List<Express> listExpress(Integer pageNum, Integer pageSize, Example example) {
		PageHelper.startPage(pageNum, pageSize);
		return expressMapper.selectByExample(example);
	}

}
