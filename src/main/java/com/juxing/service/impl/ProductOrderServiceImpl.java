package com.juxing.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.juxing.exception.ProductException;
import com.juxing.exception.ProductOrderException;
import com.juxing.mapper.ProductMapper;
import com.juxing.mapper.ProductOrderMapper;
import com.juxing.mapper.UserMapper;
import com.juxing.myenum.ProductEnum;
import com.juxing.myenum.ProductOrderEnum;
import com.juxing.pojo.Product;
import com.juxing.pojo.ProductOrder;
import com.juxing.pojo.User;
import com.juxing.povo.ProductOrderVo;
import com.juxing.service.ProductOrderService;
import com.juxing.utils.IDUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class ProductOrderServiceImpl implements ProductOrderService {
	
	@Autowired
	private ProductOrderMapper productOrderMapper;
	@Autowired
	private ProductMapper productMapper;
	@Autowired
	private UserMapper userMapper;
	
	
	/**
	 * 添加新订单
	 * @param productOrder
	 * @return
	 */
	@Override
	public int insertProductOrder(ProductOrder productOrder) {
		Product dbProduct = productMapper.selectByPrimaryKey(productOrder.getProductId());
		if(dbProduct==null) {
			throw new ProductException(ProductEnum.NOT);
		}
		if(dbProduct.getStock()<productOrder.getNumber()) {
			throw new ProductException(ProductEnum.STOKC);
		}
		// 必须是货物已经到到工作室
		if(dbProduct.getStatus()!=ProductEnum.PASS.getCode()) {
			throw new ProductException(ProductEnum.STATUS_ERROR);
		}
		productOrder.setProductOrderId(IDUtil.getStrId());
		productOrder.setCancelDesc(null);
//		0未支付跑腿费 1已支付跑腿费
		productOrder.setCodPay(0);
		productOrder.setStatus(ProductOrderEnum.WAIT.getCode());
		productOrder.setAdminId(null);
		productOrder.setTotalPrice(dbProduct.getPrice()*productOrder.getNumber());
		return productOrderMapper.insert(productOrder);
	}

	/**
	 * 更新订单【管理员】
	 * @param productOrder
	 * @return
	 */
	@Override
	public int updataUser(ProductOrder productOrder) {
		Example example = new Example(ProductOrder.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("productOrderId",productOrder.getProductOrderId());
		return productOrderMapper.updateByExampleSelective(productOrder, example);
	}
	
	/**
	 * 用户取消订单
	 */
	@Override
	public int userCancel(ProductOrder productOrder) {
		ProductOrder dbOrder = productOrderMapper.selectOne(productOrder);
		
		// 判断订单是否是等待状态
		if(dbOrder==null) {
			throw new ProductOrderException(ProductOrderEnum.NOT);
		}
		if(dbOrder.getStatus()!=ProductOrderEnum.WAIT.getCode()) {
			throw new ProductOrderException(ProductOrderEnum.STATUS_ERROR);
		}
		
		productOrder.setStatus(ProductOrderEnum.USER_CANCEL.getCode());
		return productOrderMapper.updateByPrimaryKeySelective(productOrder);
	}

	/**
	 * 根据添加查询订单
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	@Override
	public PageInfo<ProductOrderVo> list(Integer pageNum, Integer pageSize, Example example) {
		PageHelper.startPage(pageNum, pageSize);
		List<ProductOrder> list1 = productOrderMapper.selectByExample(example);
		
		//包装成vo
		List<ProductOrderVo> list2 = new ArrayList<>();
		for(ProductOrder po : list1) {
			ProductOrderVo orderVo = new ProductOrderVo();
			BeanUtils.copyProperties(po, orderVo);
			//商品
			Product dbproduct = productMapper.selectByPrimaryKey(po.getProductId());
			//买家
			User dbuser = userMapper.selectByPrimaryKey(po.getUserId());
			//管理员
			User dbadmin = userMapper.selectByPrimaryKey(po.getAdminId());
			//
			orderVo.setProduct(dbproduct);
			orderVo.setAdminUser(dbadmin);
			orderVo.setUser(dbuser);
			list2.add(orderVo);
		}
		PageInfo page = new PageInfo<>(list1);
		page.setList(list2);
		return page;
	}

	

}
