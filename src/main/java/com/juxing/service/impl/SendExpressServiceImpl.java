package com.juxing.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.juxing.mapper.ExpressMapper;
import com.juxing.mapper.SendExpressMapper;
import com.juxing.mapper.UserMapper;
import com.juxing.myenum.SendExpressEnum;
import com.juxing.pojo.Express;
import com.juxing.pojo.SendExpress;
import com.juxing.pojo.User;
import com.juxing.povo.SendExpressVo;
import com.juxing.service.SendExpressService;
import com.juxing.utils.IDUtil;

import tk.mybatis.mapper.entity.Example;

@Service
public class SendExpressServiceImpl implements SendExpressService {
	
	@Autowired
	private SendExpressMapper sendExpressMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private ExpressMapper expressMapper;

	/**
	 * 创建新的寄快递
	 * @param sendExpress
	 * @return
	 */
	@Override
	public int insert(SendExpress sendExpress) {
		sendExpress.setSendExpressId(IDUtil.getStrId());
		/**
	     * -1取消 0等待快递员揽件 1已览件寄件中 2寄件完成
	     */
		sendExpress.setStatus(0);
		return sendExpressMapper.insert(sendExpress);
	}
	
	
	/** 
	 * 取消订单
	 */
	public int cancel(String sendExpressId) {
		SendExpress sendExpress = new SendExpress();
		sendExpress.setSendExpressId(sendExpressId);
		sendExpress.setStatus(SendExpressEnum.CALCEL.getCode());
		
		return sendExpressMapper.updateByPrimaryKeySelective(sendExpress);
	}
	
	/**
	 * 揽件
	 */
	public int lj(String sendExpressId){
		SendExpress sendExpress = new SendExpress();
		sendExpress.setSendExpressId(sendExpressId);
		sendExpress.setStatus(SendExpressEnum.JJZ.getCode());
		
		return sendExpressMapper.updateByPrimaryKeySelective(sendExpress);
	}
	
	/**
	 * 寄件完成
	 */
	public int jjSuccess(String sendExpressId){
		SendExpress sendExpress = new SendExpress();
		sendExpress.setSendExpressId(sendExpressId);
		sendExpress.setStatus(SendExpressEnum.WCJJ.getCode());
		
		return sendExpressMapper.updateByPrimaryKeySelective(sendExpress);
	}

	/**
	 * 查询寄快递列表
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	@Override
	public PageInfo<Object> list(Integer pageNum, Integer pageSize, Example example) {
		PageHelper.startPage(pageNum, pageSize);
		List<SendExpress> sendExpress = sendExpressMapper.selectByExample(example);
		PageInfo pageInfo = new PageInfo<>(sendExpress);
		
		List<SendExpressVo> sendExpressVos = new ArrayList<>();
		for(SendExpress se : sendExpress) {
			SendExpressVo sendExpressVo = new SendExpressVo();
			BeanUtils.copyProperties(se, sendExpressVo);
			User dbuser = userMapper.selectByPrimaryKey(se.getUserId());
			Express dbexpress = expressMapper.selectByPrimaryKey(se.getExpressId());
			if(dbuser!=null)
				dbuser.setPassword(null);
			sendExpressVo.setUser(dbuser);
			sendExpressVo.setExpress(dbexpress);
			sendExpressVos.add(sendExpressVo);
		}
		pageInfo.setList(sendExpressVos);
		return pageInfo;
	}

}
