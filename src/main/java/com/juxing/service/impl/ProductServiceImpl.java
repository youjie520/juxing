package com.juxing.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.juxing.exception.ProductException;
import com.juxing.mapper.CategoryMapper;
import com.juxing.mapper.CommentMapper;
import com.juxing.mapper.ProductMapper;
import com.juxing.mapper.UserMapper;
import com.juxing.myenum.ProductEnum;
import com.juxing.pojo.Category;
import com.juxing.pojo.Comment;
import com.juxing.pojo.Product;
import com.juxing.pojo.User;
import com.juxing.povo.CommentVo;
import com.juxing.povo.ProductVo;
import com.juxing.service.ProductService;
import com.juxing.utils.IDUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductMapper productMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private CategoryMapper categoryMapper;
	@Autowired
	private CommentMapper commentMapper;
	
	/**
	 * 添加商品
	 * @param product
	 * @return
	 */
	@Override
	public int insertProduct(Product product) {
		product.setProductId(IDUtil.getStrId());
		product.setBrowseNum(0);
		product.setCollectionNum(0);
		
		if(product.getStatus()==ProductEnum.SELF_SELLING.getCode() || product.getStatus()==ProductEnum.AUDITING.getCode()) {
			int res = productMapper.insert(product);
			return res;
		}
		
		return -1;
	}
	
	/**
	 * 管理员更新商品
	 * @param product
	 * @return
	 */
	@Override
	public int update(Product product) {
		
		
		
		return 0;
	}
	
	/**
	 * 管理员收到货物
	 * @param productId
	 * @return
	 */
	public int takeOver(String productId) {
		Product dbProduct = productMapper.selectByPrimaryKey(productId);
		if(dbProduct==null) {
			throw new ProductException(ProductEnum.NOT);
		}
		//必须是审核的商品才行
		if(dbProduct.getStatus()!=ProductEnum.AUDITING.getCode()) {
			throw new ProductException(ProductEnum.STATUS_ERROR);
		}
		dbProduct.setStatus(ProductEnum.PASS.getCode());
		return productMapper.updateByPrimaryKeySelective(dbProduct);
	}

	/**
	 * 卖家更新商品[根据商品id和用户id查询商品]
	 */
	@Override
	public int userUpdateProduct(Product product) {
		Product queryProduct = new Product() ;
		// 根据卖家的id和商品id判断是不是这个用户的
		queryProduct.setProductId(product.getProductId());
		queryProduct.setUserId(product.getUserId());
		
		Product dbProduct = productMapper.selectOne(queryProduct);
		if(dbProduct==null) {
			throw new ProductException(ProductEnum.NOT);
		}
		
		dbProduct.setName(product.getName());
		dbProduct.setCategoryId(product.getCategoryId());
		dbProduct.setPrice(product.getPrice());
		dbProduct.setOriginalPrice(product.getOriginalPrice());
		dbProduct.setStock(product.getStock());
		dbProduct.setConditionNum(product.getConditionNum());
		dbProduct.setProductDesc(product.getProductDesc());
		dbProduct.setImgs(product.getImgs());
		return productMapper.updateByPrimaryKeySelective(dbProduct);
	}
	
	/**
	 * 下架商品 [通过商品id和用户id]
	 * @param product
	 * @return
	 */
	public int downProduct(Product product) {
		Product queryProduct = new Product() ;
		// 根据卖家的id和商品id判断是不是这个用户的
		queryProduct.setProductId(product.getProductId());
		queryProduct.setUserId(product.getUserId());
		Product dbProduct = productMapper.selectOne(queryProduct);
		if(dbProduct==null) {
			throw new ProductException(ProductEnum.NOT);
		}
		//必须是自销的商品才可以下架
		if(dbProduct.getStatus()!=ProductEnum.SELF_SELLING.getCode()) {
			throw new ProductException(ProductEnum.STATUS_ERROR);
		}
		dbProduct.setStatus(ProductEnum.LOWER_FRAME.getCode());
		
		return productMapper.updateByPrimaryKeySelective(dbProduct);
	}

	/**
	 * 根据商品id获取商品信息
	 * @param productId
	 * @return
	 */
	@Override
	public ProductVo byId(String productId) {
		Product dbProduct = productMapper.selectByPrimaryKey(productId);
		ProductVo product = new ProductVo();
		BeanUtils.copyProperties(dbProduct, product);
		//增加浏览量
		dbProduct.setBrowseNum(dbProduct.getBrowseNum()+1);
		productMapper.updateByPrimaryKeySelective(dbProduct);
		// 用户&分类
		User user = userMapper.selectByPrimaryKey(dbProduct.getUserId());
		Category category = categoryMapper.selectByPrimaryKey(dbProduct.getCategoryId());
		
		if(user!=null) {
			user.setPassword(null);
		}
		
		product.setUser(user);
		product.setCategory(category);
		
		return product;
	}
	/**
	 * 根据添加获取商品列表
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	@Override
	public PageInfo<Product> list(Integer pageNum, Integer pageSize, Example example) {
		PageHelper.startPage(pageNum, pageSize);
		List<Product> lits = productMapper.selectByExample(example);
		PageInfo<Product> page = new PageInfo<>(lits);
		return page;
	}

	/**
	 * 商品添加评论
	 * @param comment
	 * @return
	 */
	@Override
	public int addComment(Comment comment) {
		comment.setCommentId(IDUtil.getStrId());
		return commentMapper.insert(comment);
	}

	/**
	 * 根据商品Id获取评论
	 * @param pageNum
	 * @param pageSize
	 * @param productId
	 * @return
	 */
	public PageInfo<CommentVo> commentList(Integer pageNum, Integer pageSize, String productId) {
		Example example = new Example(Comment.class);
		example.setOrderByClause("create_date DESC");
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("productId", productId);
		PageHelper.startPage(pageNum, pageSize);
		List<Comment> comments = commentMapper.selectByExample(example);
		PageInfo pageInfo = new PageInfo<>(comments);
		
		List<CommentVo> commentVos = new ArrayList<>();
		for(Comment comment: comments) {
			CommentVo commentVo = new CommentVo();
			BeanUtils.copyProperties(comment, commentVo);
			User dbuser = userMapper.selectByPrimaryKey(comment.getUserId());
			commentVo.setUsername(dbuser.getName());
			commentVo.setHeadImg(dbuser.getHeadimg());
			
			commentVos.add(commentVo);
		}
		pageInfo.setList(commentVos);
		return pageInfo;
	}

	
}
