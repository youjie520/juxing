package com.juxing.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.juxing.exception.UserException;
import com.juxing.mapper.UserMapper;
import com.juxing.myenum.UserEnum;
import com.juxing.pojo.User;
import com.juxing.service.UserService;
import com.juxing.utils.IDUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;

	/**
	 * 添加新用户
	 * 
	 * @param user
	 * @return
	 */
	@Override
	public int insertUser(User user) {
		user.setUserId(IDUtil.getId());
		// 判断是否是推广来的，如果有则加+，并且判断是否符合升级要求
		if (user.getSuperior() != null) {
			User superiorUser = this.byUserId(user.getSuperior());
			if (superiorUser != null) {
				superiorUser.setExtendNumber(superiorUser.getExtendNumber() + 1);
				if (superiorUser.getExtendNumber() >= 10) {
					superiorUser.setStatus(UserEnum.Leve2.getCode());
				}
				userMapper.updateByPrimaryKeySelective(superiorUser);
			}
		}

		return userMapper.insert(user);
	}

	/**
	 * 更新用户 (拉黑修改用户状态为-1)
	 * 
	 * @param user
	 * @return
	 */
	@Override
	public int updateUser(User user) {
		return userMapper.updateByPrimaryKeySelective(user);
	}

	/**
	 * 根据用户id获取用户推广数
	 * 
	 * @param userId
	 * @return
	 */
	@Override
	public int userIdByExtendNumberBy(Long userId) {
		Example example = new Example(User.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("superior", userId);
		return userMapper.selectCountByExample(example);
	}

	/**
	 * 根据用户id查询用户
	 * 
	 * @param userId
	 * @return
	 */
	@Override
	public User byUserId(Long userId) {
		User dbuser = userMapper.selectByPrimaryKey(userId);
		if(dbuser!=null)
			dbuser.setPassword(null);
		return dbuser;
	}

	/**
	 * 根据用户openid查询用户
	 * 
	 * @param userId
	 * @return
	 */
	@Override
	public User byOpenId(String openid) {
		User user = new User();
		user.setOpenId(openid);
		User dbuser = userMapper.selectOne(user);
		if(dbuser!=null)
			dbuser.setPassword(null);
		return dbuser;
	}
	
	/**
	 * 根据用户对象查询用户
	 * @param user
	 * @return
	 */
	public User byUserObj(User user) {
		Example example = new Example(User.class);
		List<Integer> status = new ArrayList<>();
		status.add(UserEnum.Leve4.getCode());
		status.add(UserEnum.Leve5.getCode());
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("phone", user.getPhone());
		criteria.andEqualTo("password", user.getPassword());
		criteria.andIn("status", status);
//		criteria.andEqualTo("status", UserEnum.Leve4.getCode());
//		criteria.orEqualTo("status", UserEnum.Leve5.getCode());
		
		User dbuser = userMapper.selectOneByExample(example);
		if(dbuser!=null)
			dbuser.setPassword(null);
		return dbuser;
	}
	/**
	 * 根据添加查询用户
	 * 
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	@Override
	public PageInfo<User> listUser(Integer pageNum, Integer pageSize, Example example) {
		PageHelper.startPage(pageNum, pageSize);
		
		List<User> users = userMapper.selectByExample(example);
		
		for(User user : users) {
			user.setPassword(null);
		}
		
		PageInfo<User> pageInfo = new PageInfo<>(users);
		return pageInfo;
	}

}
