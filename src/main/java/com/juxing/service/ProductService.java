package com.juxing.service;

import com.github.pagehelper.PageInfo;
import com.juxing.pojo.Comment;
import com.juxing.pojo.Product;
import com.juxing.povo.CommentVo;
import com.juxing.povo.ProductVo;

import tk.mybatis.mapper.entity.Example;

/**
 * 商品service
 * @author Administrator
 *
 */
public interface ProductService {
	
	/**
	 * 添加商品
	 * @param product
	 * @return
	 */
	public int insertProduct(Product product );
	
	/**
	 * 管理员更新商品
	 * @param product
	 * @return
	 */
	public int update(Product product) ;
	
	/**
	 * 管理员收到货物
	 * @param productId
	 * @return
	 */
	public int takeOver(String productId);
	
	/**
	 * 卖家更新商品[根据商品id和用户id查询商品]
	 */
	public int userUpdateProduct(Product product);
	
	/**
	 * 卖家下架商品  [通过商品id和用户id]
	 * @param product
	 * @return
	 */
	public int downProduct(Product product);
	
	/**
	 * 根据商品id获取商品信息[分类，卖家]
	 * @param productId
	 * @return
	 */
	public ProductVo byId(String productId);
	
	/**
	 * 根据条件获取商品列表
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	public PageInfo<Product> list(Integer pageNum, Integer pageSize, Example example);

	/**
	 * 商品添加评论
	 * @param comment
	 * @return
	 */
	public int addComment(Comment comment);
	
	/**
	 * 根据商品Id获取评论
	 * @param pageNum
	 * @param pageSize
	 * @param productId
	 * @return
	 */
	public PageInfo<CommentVo> commentList(Integer pageNum, Integer pageSize,String productId);
}
