package com.juxing.service;


import com.github.pagehelper.PageInfo;
import com.juxing.pojo.User;

import tk.mybatis.mapper.entity.Example;

/**
 * 用户服务
 * @author Administrator
 *
 */
public interface UserService {
	
	/**
	 * 添加新用户
	 * @param user
	 * @return
	 */
	public int insertUser(User user);
	
	/**
	 * 更新用户 (拉黑修改用户状态为-1)
	 * @param user
	 * @return
	 */
	public int updateUser(User user);
	
	/**
	 * 根据用户id获取用户推广数
	 * @param userId
	 * @return
	 */
	public int userIdByExtendNumberBy(Long userId);
	
	
	/**
	 * 根据用户id查询用户
	 * @param userId
	 * @return
	 */
	public User byUserId(Long userId);
	
	/**
	 * 根据用户openid查询用户
	 * @param userId
	 * @return
	 */
	public User byOpenId(String openid);
	
	/**
	 * 根据用户对象查询用户
	 * @param user
	 * @return
	 */
	public User byUserObj(User user);
	
	/**
	 * 根据添加查询用户
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	public PageInfo<User> listUser(Integer pageNum,Integer pageSize,Example example);
	
	
}
