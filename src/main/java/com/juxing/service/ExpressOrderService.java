package com.juxing.service;

import com.github.pagehelper.PageInfo;
import com.juxing.pojo.ExpressOrder;
import com.juxing.pojo.User;
import com.juxing.povo.ExpressOrderVo;

import tk.mybatis.mapper.entity.Example;

public interface ExpressOrderService {
	
	/**
	 * 申请 帮拿快递订单
	 * @param expressOrder
	 * @return
	 */
	public int newExpressOrder(ExpressOrder expressOrder);
	
	/**
	 * 管理员配送中
	 * @param ExpressOrder
	 * @param user 管理员
	 * @return
	 */
	public int orderDistribution(Long ExpressOrderId,User adminUser);
	
	/**
	 * 管理员配送完成
	 * @param ExpressOrder
	 * @return
	 */
	public int orderSuccess(Long ExpressOrderId,User adminUser);
	
	/**
	 * 管理员取消订单
	 * @param ExpressOrderId
	 * @param cancelMsg
	 * @param user
	 * @param isAdmin true管理员 fase用户
	 * @return
	 */
	public int orderCancel(Long ExpressOrderId, String cancelMsg, User user,boolean isAdmin);
	
	/**
	 * 更新快递订单
	 * @param expressOrder
	 * @return
	 */
	public Integer update(ExpressOrder expressOrder, User loginUser);
	
	/**
	 * 根据订单id获取订单详细
	 * @param orderId
	 * @return
	 */
	public ExpressOrderVo byOrderId(Long orderId);
	
	/**
	 * 根据条件查询订单
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	public PageInfo<ExpressOrderVo> list(Integer pageNum,Integer pageSize,Example example);

	
}
