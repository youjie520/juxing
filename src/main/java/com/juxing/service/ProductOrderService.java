package com.juxing.service;

import com.github.pagehelper.PageInfo;
import com.juxing.pojo.ProductOrder;
import com.juxing.povo.ProductOrderVo;

import tk.mybatis.mapper.entity.Example;

public interface ProductOrderService {

	/**
	 * 添加新订单
	 * @param productOrder
	 * @return
	 */
	public int insertProductOrder(ProductOrder productOrder);
	
	
	/**
	 * 更新订单[管理员]
	 * @param productOrder
	 * @return
	 */
	public int updataUser(ProductOrder productOrder);
	
	/**
	 * 用户取消订单
	 * @param productOrder
	 * @return
	 */
	public int userCancel(ProductOrder productOrder);
	
	/**
	 * 根据添加查询订单
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	public PageInfo<ProductOrderVo> list(Integer pageNum,Integer pageSize,Example example);
	
}
