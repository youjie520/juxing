package com.juxing.service;

import com.github.pagehelper.PageInfo;

/**
 * 通用接口
 */
//@Service
public interface IService<T> {

	T selectByKey(Object key);

	int save(T entity);

	int delete(Object key);

	int updateAll(T entity);

	int updateNotNull(T entity);

	PageInfo<T> selectByExample(int pageNum, int pageSize,Object example);

	// TODO 其他...
}
