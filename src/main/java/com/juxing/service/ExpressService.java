package com.juxing.service;

import java.util.List;

import com.juxing.pojo.Express;

import tk.mybatis.mapper.entity.Example;

/**
 * 快递公司管理
 * @author Administrator
 *
 */
public interface ExpressService {
	
	/**
	 * 添加快递公司
	 * @param express
	 * @return
	 */
	public int insertExpress(Express express);
	
	
	/**
	 * 更新快递公司 (status==0不支持该快递公司)
	 * @param express
	 * @return
	 */
	public int updateExpress(Express express);
	
	/**
	 * 根据快递公司id查询快递公司信息
	 * @param expressId
	 * @return
	 */
	public Express byExpressId(Long expressId);
	
	/**
	 * 根据条件查询快递公司
	 * @param pageNum
	 * @param pageSize
	 * @param example
	 * @return
	 */
	public List<Express> listExpress(Integer pageNum,Integer pageSize,Example example);

	
}
