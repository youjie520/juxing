package com.juxing.myenum;

public enum ExpressOrderEnum implements CodeEnum{
	WAIT_DISTRIBUTION(1,"等待配送"),
	DISTRIBUTION(2,"正在配送"),
	SUCCESS_DISTRIBUTION(3,"配送完成"),
	CANCEL_DISTRIBUTION(4,"取消配送"),
//	1等待配送 2正在配送 3配送完成 4配送取消
	STATUS_ERROR(1000,"状态错误"),
	ORDER_ADMIN_ERROR(1002,"该订单不是这个管理员的"),
	NOT_EXPRESS_ORDER(1003,"订单不存在"),
	NOT_AUTHORITY(1004,"快递订单权限不够"),
	REPEAT_SUBMIT(1005,"快递订单重复提交"),
	NOT_USER(1006,"订单不是你的"),
	SUPPORT_ERROR(1007,"不支持该快递公司")
	;
	
	private Integer code;
	private String message;
	private ExpressOrderEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
