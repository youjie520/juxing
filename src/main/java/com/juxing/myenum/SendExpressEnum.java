package com.juxing.myenum;

public enum SendExpressEnum implements CodeEnum{
//	-1取消 0等待快递员揽件 1已览件寄件中 2寄件完成
	CALCEL(-1,"取消"),
	DDLJ(0,"等待快递员揽件"),
	JJZ(1,"已览件寄件中"),
	WCJJ(2,"寄件完成"),
	;
	
	private Integer code;
	private String message;
	
	private SendExpressEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
