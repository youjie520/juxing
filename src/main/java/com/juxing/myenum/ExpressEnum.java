package com.juxing.myenum;

public enum ExpressEnum implements CodeEnum{
	OFFNORMAL(10,"不支持"),
	NORMAL(11,"支持"),

	;
	private Integer code;
	private String message;
	
	private ExpressEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
