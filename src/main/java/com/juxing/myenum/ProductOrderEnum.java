package com.juxing.myenum;

public enum ProductOrderEnum implements CodeEnum{
	WAIT(0,"等待交易"),
	TRANSACTION(1,"交易中"),
	SUCCESS(2,"交易成功"),
	FAIL(3,"交易失败"),
	USER_CANCEL(4,"用户取消交易"),
	
	
	NOT(1000,"订单不存在"),
	STATUS_ERROR(1001,"订单状态错误")
	;
	
	private Integer code;
	private String message;
	
	private ProductOrderEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
