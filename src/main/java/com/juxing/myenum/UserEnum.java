package com.juxing.myenum;

public enum UserEnum implements CodeEnum{
	Leve(-1,"拉黑"),
	Leve1(0,"普通"),
	Leve2(1,"中级"),
	Leve3(2,"高级"),
	Leve4(3,"管理员"),
	Leve5(4,"超级管理员"),
	
	LOGIN_ERROR(1000,"微信登录失败"),
	USER_REG(1002,"用户已注册"),
	NOT_REG(1003,"用户未注册"),
	NOT_LONG(1004,"用户没有登录"),
	USER_EXPIRE(1005,"用户身份过期"),
	PHONE_PASSWORD_ERROR(1006,"手机号或密码错误"),
	OP_REEOR(1007,"操作权限不够"),
	;
	
	private Integer code;
	private String message;
	
	private UserEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
