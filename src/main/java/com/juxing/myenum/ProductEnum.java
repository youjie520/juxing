package com.juxing.myenum;

public enum ProductEnum implements CodeEnum{
	//     
	LOWER_FRAME(-1,"下架"), //-1下架 
	SELF_SELLING(0,"自销"), // 0用户自己联系销售(货物还没到工作室)
	AUDITING(1,"审核中"),    // 1平台销售审核中(货物还没到工作室)
	PASS(2,"通过"),			// 2审核通过(货物已经到工作室了) 
	WAITING_TAKE_BACK(3,"等待取回"),
	TAKE_BACK(4,"已取回"),
	
	
	ADD_REEOR(1000,"发布商品失败"),
	NOT(1001,"商品不存在"),
	STOKC(1002,"库存不足"),
	STATUS_ERROR(1003,"状态错误")
	;
	
	private Integer code;
	private String message;
	
	private ProductEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
