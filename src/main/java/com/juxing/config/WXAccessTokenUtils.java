package com.juxing.config;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.juxing.utils.JsonUtils;

import cn.hutool.http.HttpUtil;

@Configuration
public class WXAccessTokenUtils {
	
	@Value("${wx.appId}")
	private String appId;
	@Value("${wx.secret}")
	private String secret;
	
	private String accessToken= null;
	
	//获取accessToken
	public String getAccessToken() {
		//
		String result1= HttpUtil.get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential"+
									"&appid="+appId+"&secret="+secret);
		Map map = JsonUtils.jsonToPojo(result1, Map.class);
		if(map.get("errcode")!=null && !map.get("errcode").equals("0")) {
			return "token获取错误:"+map.get("errcode");
		}
		return result1;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
