package com.juxing.config;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private JavaMailSender mailSender;// spring 提供的邮件发送类

	@Value("${spring.mail.fromMail.addr}")
	private String from;

	/**
	 * 
	 * @param to		设置收件人
	 * @param subject	设置主题
	 * @param content	设置内容
	 */
	public void sendHtmlEmail(String to, String subject, String content) {
		MimeMessage message = mailSender.createMimeMessage();// 创建一个MINE消息

		try {
			// true表示需要创建一个multipart message
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(from);//设置发送人
			helper.setTo(to);//设置收件人
			helper.setSubject(subject);//设置主题
			helper.setText(content, true); //设置内容

			mailSender.send(message);//执行发送邮件
			logger.info("html邮件发送成功");
		} catch (MessagingException e) {
			logger.error("发送html邮件时发生异常！", e);
		}

	}
}
