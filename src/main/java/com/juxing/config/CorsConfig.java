package com.juxing.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Created by myz on 2017/7/10.
 *
 * 璁剧疆璺ㄥ煙璇锋眰
 */
@Configuration
public class CorsConfig {
    private CorsConfiguration buildConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*"); // 1 璁剧疆璁块棶婧愬湴鍧�
        corsConfiguration.addAllowedHeader("*"); // 2 璁剧疆璁块棶婧愯姹傚ご
        corsConfiguration.addAllowedMethod("*"); // 3 璁剧疆璁块棶婧愯姹傛柟娉�
        return corsConfiguration;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", buildConfig()); // 4 瀵规帴鍙ｉ厤缃法鍩熻缃�
        return new CorsFilter(source);
    }
}