package com.juxing.pojo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Table(name = "`product`")
public class Product {
    @Id
    @Column(name = "`product_id`")
    private String productId;
    
    @NotNull
    @Column(name = "`category_id`")
    private Long categoryId;

    @Column(name = "`user_id`")
    private Long userId;
    
    @NotBlank
    @Column(name = "`name`")
    private String name;
    
    @DecimalMin(value="0")
    @Column(name = "`price`")
    private Integer price;
    
    @DecimalMin(value="0")
    @Column(name = "`original_price`")
    private Integer originalPrice;

    @DecimalMin(value="1")
    @Column(name = "`stock`")
    private Integer stock;

    @Column(name = "`browse_num`")
    private Integer browseNum;

    @Column(name = "`collection_num`")
    private Integer collectionNum;

    /**
     * 几成新
     */
    @DecimalMin(value="1")
    @DecimalMax("10")
    @Column(name = "`condition_num`")
    private Integer conditionNum;

    @NotBlank
    @Column(name = "`product_desc`")
    private String productDesc;

    /**
     * 第一张为封面
     */
//    @NotBlank
    @Column(name = "`imgs`")
    private String imgs;

    /**
     * -1下架    0用户自己联系销售(货物还没到工作室)  1平台销售审核中(货物还没到工作室)  2审核通过(货物已经到工作室了) 3等待取回    4用户取回
     */
    @NotNull
    @Column(name = "`status`")
    private Integer status;

    @Column(name = "`create_date`",insertable=false)
    private Date createDate;

    @Column(name = "`update_date`",updatable=false)
    private Date updateDate;

    /**
     * @return product_id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return category_id
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * @return original_price
     */
    public Integer getOriginalPrice() {
        return originalPrice;
    }

    /**
     * @param originalPrice
     */
    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    /**
     * @return stock
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * @param stock
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * @return browse_num
     */
    public Integer getBrowseNum() {
        return browseNum;
    }

    /**
     * @param browseNum
     */
    public void setBrowseNum(Integer browseNum) {
        this.browseNum = browseNum;
    }

    /**
     * @return collection_num
     */
    public Integer getCollectionNum() {
        return collectionNum;
    }

    /**
     * @param collectionNum
     */
    public void setCollectionNum(Integer collectionNum) {
        this.collectionNum = collectionNum;
    }

    /**
     * 获取几成新
     *
     * @return condition_num - 几成新
     */
    public Integer getConditionNum() {
        return conditionNum;
    }

    /**
     * 设置几成新
     *
     * @param conditionNum 几成新
     */
    public void setConditionNum(Integer conditionNum) {
        this.conditionNum = conditionNum;
    }

    /**
     * @return product_desc
     */
    public String getProductDesc() {
        return productDesc;
    }

    /**
     * @param productDesc
     */
    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    /**
     * 获取第一张为封面
     *
     * @return imgs - 第一张为封面
     */
    public String getImgs() {
        return imgs;
    }

    /**
     * 设置第一张为封面
     *
     * @param imgs 第一张为封面
     */
    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    /**
     * 获取0审核中(货物还没到工作室)  1审核不通过  2审核通过(货物已经到工作室了)  
     *
     * @return status - 0审核中(货物还没到工作室)  1审核不通过  2审核通过(货物已经到工作室了)  
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置0审核中(货物还没到工作室)  1审核不通过  2审核通过(货物已经到工作室了)  
     *
     * @param status 0审核中(货物还没到工作室)  1审核不通过  2审核通过(货物已经到工作室了)  
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}