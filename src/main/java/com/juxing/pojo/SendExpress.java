package com.juxing.pojo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Table(name = "`send_express`")
public class SendExpress {
    @Id
    @Column(name = "`send_express_id`")
    private String sendExpressId;
    
    @Column(name = "`user_id`")
    private Long userId;

    @NotNull
    @Column(name = "`express_id`")
    private Long expressId;

    @NotBlank
    @Column(name = "`sender`")
    private String sender;

    @NotBlank
    @Column(name = "`consignee`")
    private String consignee;

    @NotBlank
    @Column(name = "`express_type`")
    private String expressType;

    @NotBlank
    @Column(name = "`weight`")
    private String weight;

    
    @Column(name = "`send_desc`")
    private String sendDesc;

    /**
     * -1取消 0等待快递员揽件 1已览件寄件中 2寄件完成
     */
    @Column(name = "`status`")
    private Integer status;

    /**
     * @return send_express_id
     */
    public String getSendExpressId() {
        return sendExpressId;
    }

    /**
     * @param sendExpressId
     */
    public void setSendExpressId(String sendExpressId) {
        this.sendExpressId = sendExpressId;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return express_id
     */
    public Long getExpressId() {
        return expressId;
    }

    /**
     * @param expressId
     */
    public void setExpressId(Long expressId) {
        this.expressId = expressId;
    }

    /**
     * @return sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @param sender
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * @return consignee
     */
    public String getConsignee() {
        return consignee;
    }

    /**
     * @param consignee
     */
    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    /**
     * @return express_type
     */
    public String getExpressType() {
        return expressType;
    }

    /**
     * @param expressType
     */
    public void setExpressType(String expressType) {
        this.expressType = expressType;
    }

    /**
     * @return weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * @return send_desc
     */
    public String getSendDesc() {
        return sendDesc;
    }

    /**
     * @param sendDesc
     */
    public void setSendDesc(String sendDesc) {
        this.sendDesc = sendDesc;
    }

    /**
     * 获取-1取消 0等待快递员揽件 1已览件寄件中 2寄件完成
     *
     * @return status - -1取消 0等待快递员揽件 1已览件寄件中 2寄件完成
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置-1取消 0等待快递员揽件 1已览件寄件中 2寄件完成
     *
     * @param status -1取消 0等待快递员揽件 1已览件寄件中 2寄件完成
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

	@Override
	public String toString() {
		return "SendExpress [sendExpressId=" + sendExpressId + ", userId=" + userId + ", expressId=" + expressId
				+ ", sender=" + sender + ", consignee=" + consignee + ", expressType=" + expressType + ", weight="
				+ weight + ", sendDesc=" + sendDesc + ", status=" + status + "]";
	}
    
    
}