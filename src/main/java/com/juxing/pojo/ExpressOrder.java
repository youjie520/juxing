package com.juxing.pojo;

import java.util.Date;
import javax.persistence.*;

import com.juxing.myenum.ExpressOrderEnum;
import com.juxing.utils.EnumUtils;

@Table(name = "`express_order`")
public class ExpressOrder {
    @Id
    @Column(name = "`order_id`")
    private Long orderId;

    @Column(name = "`express_id`")
    private Long expressId;

    @Column(name = "`user_id`")
    private Long userId;
    
    @Column(name = "`admin_id`")
    private Long adminId;

    //领取码
    @Column(name = "`pick_up_code`")
    private String pickUpCode;

    @Column(name = "`name`")
    private String name;

    @Column(name = "`phone`")
    private String phone;

    @Column(name = "`dorm_number`")
    private String dormNumber;

    @Column(name = "`user_desc`")
    private String userDesc;

    @Column(name = "`cancel_desc`")
    private String cancelDesc;

    /**
     * 1等待配送 2正在配送 3配送完成 4配送取消
     */
    @Column(name = "`status`")
    private Integer status;
    
    @Transient
    private String statusStr;

    @Column(name = "`create_date`",insertable=false)
    private Date createDate;

    @Column(name = "`update_date`",updatable=false)
    private Date updateDate;

    /**
     * @return order_id
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * @param orderId
     */
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * @return express_id
     */
    public Long getExpressId() {
        return expressId;
    }

    /**
     * @param expressId
     */
    public void setExpressId(Long expressId) {
        this.expressId = expressId;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return pick_up_code
     */
    public String getPickUpCode() {
        return pickUpCode;
    }

    /**
     * @param pickUpCode
     */
    public void setPickUpCode(String pickUpCode) {
        this.pickUpCode = pickUpCode;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return dorm_number
     */
    public String getDormNumber() {
        return dormNumber;
    }

    /**
     * @param dormNumber
     */
    public void setDormNumber(String dormNumber) {
        this.dormNumber = dormNumber;
    }

    /**
     * @return user_desc
     */
    public String getUserDesc() {
        return userDesc;
    }

    /**
     * @param userDesc
     */
    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    /**
     * @return cancel_desc
     */
    public String getCancelDesc() {
        return cancelDesc;
    }

    /**
     * @param cancelDesc
     */
    public void setCancelDesc(String cancelDesc) {
        this.cancelDesc = cancelDesc;
    }

    /**
     * 获取1等待配送 2正在配送 3配送完成 4配送取消
     *
     * @return status - 1等待配送 2正在配送 3配送完成 4配送取消
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置1等待配送 2正在配送 3配送完成 4配送取消
     *
     * @param status 1等待配送 2正在配送 3配送完成 4配送取消
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public String getStatusStr() {
		return EnumUtils.getEunm(this.status, ExpressOrderEnum.class).getMessage();
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	
	
    
}