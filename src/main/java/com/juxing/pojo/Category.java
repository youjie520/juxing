package com.juxing.pojo;

import java.util.Date;
import javax.persistence.*;

@Table(name = "`category`")
public class Category {
    @Id
    @Column(name = "`category_id`")
    private Long categoryId;

    @Column(name = "`name`")
    private String name;

    /**
     * 10 下架  11上架
     */
    @Column(name = "`status`")
    private Integer status;

    @Column(name = "`create_date`",insertable=false)
    private Date createDate;

    @Column(name = "`update_date`",updatable=false)
    private Date updateDate;

    /**
     * @return category_id
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取0 下架 1上架
     *
     * @return status - 0 下架 1上架
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置0 下架 1上架
     *
     * @param status 0 下架 1上架
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}