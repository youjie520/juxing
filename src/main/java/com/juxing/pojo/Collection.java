package com.juxing.pojo;

import java.util.Date;
import javax.persistence.*;

@Table(name = "`collection`")
public class Collection {
    @Id
    @Column(name = "`collection_id`")
    private Long collectionId;

    @Column(name = "`product_id`")
    private String productId;

    @Column(name = "`user_id`")
    private Long userId;

    @Column(name = "`create_date`",insertable=false)
    private Date createDate;


    /**
     * @return collection_id
     */
    public Long getCollectionId() {
        return collectionId;
    }

    /**
     * @param collectionId
     */
    public void setCollectionId(Long collectionId) {
        this.collectionId = collectionId;
    }

    /**
     * @return product_id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}