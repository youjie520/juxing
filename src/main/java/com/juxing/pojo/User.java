package com.juxing.pojo;

import java.util.Date;
import javax.persistence.*;

import com.juxing.myenum.UserEnum;
import com.juxing.utils.EnumUtils;

@Table(name = "`user`")
public class User {
    @Id
    @Column(name = "`user_id`")
    private Long userId;

    @Column(name = "`open_id`")
    private String openId;

    @Column(name = "`name`")
    private String name;
    
    @Column(name = "`headimg`")
    private String headimg;

    @Column(name = "`phone`")
    private String phone;
    
    @Column(name = "`password`")
    private String password;

    /**
     * 宿舍号
     */
    @Column(name = "`dorm_number`")
    private String dormNumber;
     
    /**
     * 推广数
     */
    @Column(name = "`extend_number`")
    private Integer extendNumber;
    /**
     * 是否是通过推广来的
     */
    @Column(name = "`superior`")
    private Long superior;

    /**
     * -1拉黑 0普通 1中级 2高级 3管理员
     */
    @Column(name = "`status`")
    private Integer status;
    
    @Transient
    private String statusStr;

    @Column(name = "`create_date`",insertable=false)
    private Date createDate;

    @Column(name = "`update_date`",updatable=false)
    private Date updateDate;

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return open_id
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * @param openId
     */
    public void setOpenId(String openId) {
        this.openId = openId;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取宿舍号
     *
     * @return dorm_number - 宿舍号
     */
    public String getDormNumber() {
        return dormNumber;
    }

    /**
     * 设置宿舍号
     *
     * @param dormNumber 宿舍号
     */
    public void setDormNumber(String dormNumber) {
        this.dormNumber = dormNumber;
    }

    /**
     * 获取是否是通过推广来的
     *
     * @return superior - 是否是通过推广来的
     */
    public Long getSuperior() {
        return superior;
    }

    /**
     * 设置是否是通过推广来的
     *
     * @param superior 是否是通过推广来的
     */
    public void setSuperior(Long superior) {
        this.superior = superior;
    }

    /**
     * 获取-1拉黑 0普通 1中级 2高级 3管理员
     *
     * @return status - -1拉黑 0普通 1中级 2高级 3管理员
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置-1拉黑 0普通 1中级 2高级 3管理员
     *
     * @param status -1拉黑 0普通 1中级 2高级 3管理员
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

	public Integer getExtendNumber() {
		return extendNumber;
	}

	public void setExtendNumber(Integer extendNumber) {
		this.extendNumber = extendNumber;
	}
	
	public String getStatusStr() {
		return EnumUtils.getEunm(this.getStatus(), UserEnum.class).getMessage();
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHeadimg() {
		return headimg;
	}

	public void setHeadimg(String headimg) {
		this.headimg = headimg;
	}
	
    
}