package com.juxing.pojo;

import java.util.Date;
import javax.persistence.*;

@Table(name = "`product_order`")
public class ProductOrder {
    @Id
    @Column(name = "`product_order_id`")
    private String productOrderId;

    @Column(name = "`product_id`")
    private String productId;
    
    @Column(name = "`user_id`")
    private Long userId;
    /**
     * 接单管理员id
     */
    @Column(name = "`admin_id`")
    private Long adminId;

    @Column(name = "`total_price`")
    private Integer totalPrice;

    @Column(name = "`number`")
    private Integer number;

    @Column(name = "`order_desc`")
    private String orderDesc;

    @Column(name = "`cancel_desc`")
    private String cancelDesc;

    /**
     * 0自提 1货到付款
     */
    @Column(name = "`cod`")
    private Integer cod;

    /**
     * 0未支付跑腿费 1已支付跑腿费
     */
    @Column(name = "`cod_pay`")
    private Integer codPay;

    /**
     * 0 等待交易 1交易中 2交易成功 3交易失败 4用户取消交易
     */
    @Column(name = "`status`")
    private Integer status;

    @Column(name = "`create_date`",insertable=false)
    private Date createDate;

    @Column(name = "`update_date`",updatable=false)
    private Date updateDate;

    /**
     * @return product_order_id
     */
    public String getProductOrderId() {
        return productOrderId;
    }

    /**
     * @param productOrderId
     */
    public void setProductOrderId(String productOrderId) {
        this.productOrderId = productOrderId;
    }

    /**
     * @return product_id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * 获取接单管理员id
     *
     * @return admin_id - 接单管理员id
     */
    public Long getAdminId() {
        return adminId;
    }

    /**
     * 设置接单管理员id
     *
     * @param adminId 接单管理员id
     */
    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    /**
     * @return total_price
     */
    public Integer getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     */
    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return number
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * @param number
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * @return order_desc
     */
    public String getOrderDesc() {
        return orderDesc;
    }

    /**
     * @param orderDesc
     */
    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    /**
     * @return cancel_desc
     */
    public String getCancelDesc() {
        return cancelDesc;
    }

    /**
     * @param cancelDesc
     */
    public void setCancelDesc(String cancelDesc) {
        this.cancelDesc = cancelDesc;
    }

    /**
     * 获取0自提 1货到付款
     *
     * @return cod - 0自提 1货到付款
     */
    public Integer getCod() {
        return cod;
    }

    /**
     * 设置0自提 1货到付款
     *
     * @param cod 0自提 1货到付款
     */
    public void setCod(Integer cod) {
        this.cod = cod;
    }

    /**
     * 获取0未支付跑腿费 1已支付跑腿费
     *
     * @return cod_pay - 0未支付跑腿费 1已支付跑腿费
     */
    public Integer getCodPay() {
        return codPay;
    }

    /**
     * 设置0未支付跑腿费 1已支付跑腿费
     *
     * @param codPay 0未支付跑腿费 1已支付跑腿费
     */
    public void setCodPay(Integer codPay) {
        this.codPay = codPay;
    }

    /**
     * 获取0 等待交易 1交易中 2交易成功 3交易失败 4用户取消交易
     *
     * @return status - 0 等待交易 1交易中 2交易成功 3交易失败 4用户取消交易
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置0 等待交易 1交易中 2交易成功 3交易失败 4用户取消交易
     *
     * @param status 0 等待交易 1交易中 2交易成功 3交易失败 4用户取消交易
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
    
}