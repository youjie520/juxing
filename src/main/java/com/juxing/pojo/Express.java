package com.juxing.pojo;

import java.util.Date;
import javax.persistence.*;

import com.juxing.myenum.ExpressEnum;
import com.juxing.myenum.UserEnum;
import com.juxing.utils.EnumUtils;

@Table(name = "`express`")
public class Express {
    @Id
    @Column(name = "`express_id`")
    private Long expressId;

    @Column(name = "`addr`")
    private String addr;

    @Column(name = "`number`")
    private Integer number;

    @Column(name = "`name`")
    private String name;

    @Column(name = "`img`")
    private String img;

    /**
     * 1/2/3 表示几级用户可以操作 (用用户的枚举)
     */
    @Column(name = "`type`")
    private Integer type;

    /**
     * 0不支持 1支持
     */
    @Column(name = "`status`")
    private Integer status;

    @Column(name = "`create_date`",insertable=false)
    private Date createDate;

    @Column(name = "`update_date`",updatable=false)
    private Date updateDate;

    /**
     * @return express_id
     */
    public Long getExpressId() {
        return expressId;
    }

    /**
     * @param expressId
     */
    public void setExpressId(Long expressId) {
        this.expressId = expressId;
    }

    /**
     * @return addr
     */
    public String getAddr() {
        return addr;
    }

    /**
     * @param addr
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * @return number
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * @param number
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * 获取1/2/3 表示几级用户可以操作
     *
     * @return type - 1/2/3 表示几级用户可以操作
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置1/2/3 表示几级用户可以操作
     *
     * @param type 1/2/3 表示几级用户可以操作
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取0不支持 1支持
     *
     * @return status - 0不支持 1支持
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置0不支持 1支持
     *
     * @param status 0不支持 1支持
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return update_date
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
    public String getTypeStr() {
    	UserEnum eunm = EnumUtils.getEunm(this.getType(), UserEnum.class);
    	return eunm.getMessage();
    }
}