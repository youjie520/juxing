package com.juxing.utils;

import java.util.Random;

public class IDUtil {
	public static Long getId() {
		Random random = new Random();
		return System.currentTimeMillis()+random.nextInt(1000);
	}
	
	public static String getStrId() {
		Random random = new Random();
		return new String(System.currentTimeMillis()+random.nextInt(1000)+"");
	}
}
