package com.juxing.utils;

import com.juxing.myenum.CodeEnum;

public class EnumUtils {
	
	/**
	 * 只要枚举实现了CodeEnum接口，就可以通过枚举值获取枚举对象
	 * @param code
	 * @param baseEnum
	 * @return
	 */
	public static <T extends CodeEnum> T getEunm(Integer code, Class<T> baseEnum) {
		if(code==null)
			return null;
		for(T tenum: baseEnum.getEnumConstants()){
			if(code.equals(tenum.getCode())) {
				return tenum;
			}
		}
		
		return null;
	} 
}
