package com.juxing.utils;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import com.juxing.exception.UserException;
import com.juxing.myenum.UserEnum;
import com.juxing.pojo.User;


@Component
public class UserRedisUtil {
	@Autowired
	private StringRedisTemplate redisTemplate;

	public User getUser(String token) {
		// 获取登录用户
		// 用户没有登录错误代码为 400
		String userString = redisTemplate.opsForValue().get(token);
		if (userString == null) {
			throw new UserException(UserEnum.NOT_LONG);	// 过期或者token错误 客户端因要重新登录
		}
		redisTemplate.expire(token, 84000, TimeUnit.SECONDS);	// 每用一次就更新过期时间为一天
		return JsonUtils.jsonToPojo(userString, User.class);
	}
}
