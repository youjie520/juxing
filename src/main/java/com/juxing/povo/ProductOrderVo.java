package com.juxing.povo;

import com.juxing.pojo.Product;
import com.juxing.pojo.ProductOrder;
import com.juxing.pojo.User;

/**
 * 订单VO
 * @author Administrator
 *
 */
public class ProductOrderVo extends ProductOrder{
	
	/**
	 * 买家
	 */
	private User user;
	/**
	 * 操作管理员
	 */
	private User AdminUser;
	
	/**
	 * 商品
	 */
	private Product product;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getAdminUser() {
		return AdminUser;
	}

	public void setAdminUser(User adminUser) {
		AdminUser = adminUser;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
}
