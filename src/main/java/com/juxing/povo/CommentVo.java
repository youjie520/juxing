package com.juxing.povo;

import com.juxing.pojo.Comment;

public class CommentVo extends Comment{
	
	private String username;
	private String headImg;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getHeadImg() {
		return headImg;
	}
	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	
}
