package com.juxing.povo;

import com.juxing.pojo.Express;
import com.juxing.pojo.SendExpress;
import com.juxing.pojo.User;

public class SendExpressVo extends SendExpress {
	private User user;
	private Express express;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Express getExpress() {
		return express;
	}
	public void setExpress(Express express) {
		this.express = express;
	}
	
	
}
