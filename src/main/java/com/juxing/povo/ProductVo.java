package com.juxing.povo;

import com.juxing.pojo.Category;
import com.juxing.pojo.Product;
import com.juxing.pojo.User;

/**
 * 商品信息包含【分类，用户信息】
 * @author Administrator
 *
 */
public class ProductVo extends Product{
	
	private Category category;
	private User user;
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
