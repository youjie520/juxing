package com.juxing.povo;

import com.juxing.pojo.Express;
import com.juxing.pojo.ExpressOrder;
import com.juxing.pojo.User;

public class ExpressOrderVo extends ExpressOrder{
	private User user;
	private Express express;
	private User adminUser;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Express getExpress() {
		return express;
	}
	public void setExpress(Express express) {
		this.express = express;
	}
	public User getAdminUser() {
		return adminUser;
	}
	public void setAdminUser(User adminUser) {
		this.adminUser = adminUser;
	}
	
}
